def countSwaps(a):
    # Write your code here
    swapped = False
    count = 0

    for _ in range(len(a) - 1, 0, -1):
        for j in range(_):
            if a[j] > a[j + 1]:
                a[j], a[j + 1] = a[j + 1], a[j]
                swapped = True
                count += 1
        if swapped:
            continue
        else:
            break

    return {"count": count, "updated_array": a}


if __name__ == '__main__':
    n = int(input().strip())

    a = list(map(int, input().rstrip().split()))

    result = countSwaps(a)

    print(f"Array is sorted in {result['count']} swaps.")
    print(f"First Element: {result['updated_array'][0]}")
    print(f"Last Element: {result['updated_array'][-1]}")
