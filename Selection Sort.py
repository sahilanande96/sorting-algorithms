def selection_sort(a):
    n = len(a)

    for _ in range(n - 1):
        min_index = _
        for j in range(_ + 1, n):
            if a[j] < a[min_index]:
                min_index = j

        a[min_index], a[_] = a[_], a[min_index]

    return a


L = [6, 4, 8, 9, 1]
sorted_array = selection_sort(L)
print(f"Sorted Array is {sorted_array}")
